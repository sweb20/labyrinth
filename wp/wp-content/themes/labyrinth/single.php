<?php get_header(); ?>

<section class="sec__mv">
  <h2 class="sec__ttl"><span>BLOG</span></h2>
</section>

<div class="blog contents">
  <div class="blog__inner">
    <?php
    if ( have_posts() ) :
      echo '<div class="blog__post">';
      while ( have_posts() ) :
        the_post();
    ?>
			<p class="blog__day"><?php echo get_the_date(); ?></p>
      <h4 class="blog__ttl blog__ttl--post"><?php the_title(); ?></h4>
      <div>
        
        <?php
              // カテゴリーの取得

              // $terms     = get_the_terms( get_the_id(), 'blog_cat' );
              $terms     = get_the_category();
              $cat       = array();

              if ( $terms ) {
                echo '<p class="blog__cat">';
                // カテゴリーをセット
                foreach ( $terms as $key => $value ) {
                  $cat[] = $value->name;
                  echo '<span><a href="/'.$value->taxonomy.'/'.$value->slug.'/">'.$value->name.'</a></span>';
                }
                echo '</p>';
              }
            ?>
      </div>
     
      <div class="blog__post__inner">
        <p class="blog__img blog__img--post"><?php
        // アイキャッチ
        if ( has_post_thumbnail( $post->ID ) ) {
          echo the_post_thumbnail( 'full' );
          } else {
            echo '<img src="'.get_template_directory_uri().'/img/blog/no__image.jpg" alt="no__image">';
          }
        ?></p>
        <div class="blog__post__txt">
         <?php the_content(); ?>
        </div>
     
      </div>
      <?php endwhile; endif; ?>
    </div>
    <div class="page__single">
      <p><?php previous_post_link('%link', 'Previous', TRUE, ''); ?></p>
      <p><?php next_post_link('%link', 'Next', TRUE, ''); ?></p>
    </div>

    <?php comments_template(); ?>
  </div>

  <aside class="side">
    <div class="side__inner">
			<?php dynamic_sidebar( 'blog__widget' ); ?>
    </div>
  </aside>
</div>
<?php get_footer(); ?>
$(function () {
  $('#sp_btn').on('click',function(e){
    $('.head_nav_sp').toggleClass('active');
    $('header').toggleClass('active');
    $('.sp_btn').toggleClass('close');
    e.stopPropagation();
  });
  $('.link_remove').on('click',function(e){
    $('.head_nav_sp').removeClass('active');
    $('header').removeClass('active');
    $('.sp_btn').removeClass('close');
    e.stopPropagation();
  });
  $(window).scroll(function(e) {
    if ($('.sp_btn').hasClass('close')) {
      $('.head_nav_sp').removeClass('active');
      $('header').removeClass('active');
      $('.sp_btn').removeClass('close');
    }
  });
  $(window).resize(function () {
    if ($('.sp_btn').hasClass('close')) {
      $('.head_nav_sp').removeClass('active');
      $('header').removeClass('active');
      $('.sp_btn').removeClass('close');
    }
  });
});

$(function(){
  $('a[href *= "#"]').on('click',function(){
    var speed     = 500;
    var target    = $(this).attr('href').split('#');
    var target    = target ? "#" + target[1] : "#";
    var targetTop = target != "#" ? $( target ).offset().top : 0;
    $('html,body').stop().animate({
        scrollTop: targetTop,
      },
      {
        'duration': speed,
        'easing': 'swing',
    });
    return false;
  });
});

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <title>奇妙なラビリンスへの招待状</title>
  <!-- icon -->
  <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/common/favicon.ico">
  <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/img/common/apple-touch-icon.png" sizes="180x180">
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-5NCHLNX');</script>
  <!-- End Google Tag Manager -->
  <!-- IE対策 -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge"><?php wp_head(); ?>
  <!-- その他設定 -->
  <meta name="format-detection" content="telephone=no">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
  <!-- CSS -->
  <link href="https://fonts.googleapis.com/css2?family=Lato:wght@700;900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/normalize.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/common.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/index.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/parts.css">
  <?php
    if ( is_home() ) {
      echo '<link rel="stylesheet" href="'.get_template_directory_uri().'/css/info.css">';
    }
    elseif ( is_category() || is_archive() ) {
      echo '<link rel="stylesheet" href="'.get_template_directory_uri().'/css/info.css">';
    }
    elseif ( is_post_type_archive( get_post_type() ) ) {
      echo '<link rel="stylesheet" href="'.get_template_directory_uri().'/css/'.get_post_type().'.css">';
    }
    elseif ( is_singular( get_post_type() ) && is_single() ) {
      echo '<link rel="stylesheet" href="'.get_template_directory_uri().'/css/info.css">';
    }
    elseif ( is_404() ) {
      echo '<link rel="stylesheet" href="'.get_template_directory_uri().'/css/404.css">';
    }
    else {
      echo '<link rel="stylesheet" href="'.get_template_directory_uri().'/css/'.get_page_uri( $post->ID ).'.css">';
    }
  ?>
  
</head>
<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5NCHLNX"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <header class="header">
    <div class="header_wrap flex ag_middle jt_between">
      <div class="logo_area">
        <h1><a href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/common/header_logo.svg" alt="奇妙なラビリンスへの招待状 ロゴ"></a></h1>
      </div>
      <div class="nav_area">
        <nav class="header_nav">
          <ul class="flex">
            <li><a href="/">トップ</a></li>
            <li><a href="/#news_section">お知らせ</a></li>
            <li><a href="/#introduction_section">イントロダクション</a></li>
            <li><a href="/#performance_section">公演情報</a></li>
            <!-- <li><a href="/#message_section1">予告編映像</a></li> -->
            <li><a href="/#message_section2">主題歌</a></li>
            <li><a href="/#gllery">ギャラリー</a></li>
          </ul>
        </nav>
      </div>
      <div id="sp_btn" class="sp_btn"><span></span></div>
    </div>
    <nav class="sp_nav">
      <ul>
        <li><a href="/">トップ</a></li>
        <li><a href="/#news_section">お知らせ</a></li>
        <li><a href="/#introduction_section">イントロダクション</a></li>
        <li><a href="/#performance_section">公演情報</a></li>
        <!-- <li><a href="/#message_section1">予告編映像</a></li> -->
        <li><a href="/#message_section2">主題歌</a></li>
        <li><a href="/#gllery">ギャラリー</a></li>
      </ul>
    </nav>
  </header>
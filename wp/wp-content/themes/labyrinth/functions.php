<?php
// 初期設定関数群(主に管理画面)
get_template_part('function/init');

// headerで自動生成される不要なタグを削除する関数群
get_template_part('function/cleanup');

/**
 *
 * 01.0 - パンくず
 * 02.0 - カスタム投稿タイプ
 * 02.1 - カスタム投稿の追加
 * 02.2 - カスタム投稿タイプのタクソノミー登録
 * 02.3 - カスタム投稿タイプのスラッグをIDへ変更
 * 03.0 - アーカイブページの記事ループ外でも、通常の記事ループ内でもpost_typeを取得できるようにした関数
 * 04.0 - 子ページであるかのチェック(true -> 親ページの ID を返す。)
 * 05.0 - 親スラッグの取得
 *   05.1 - 直上の親を返す
 *   05.2 - 一番上の親を返す
 * 06.0 - アイキャッチのサイズ追加
 * 07.0 - ユーザー検索をリダイレクト
 * 08.0 - All in one SEO 除外系
 * 09.0 - デフォルトギャラリーのCSS停止
 */

//---------------------------------------------------------------------------------------------------
/**
 * 01.0 - パンくず
 */
//---------------------------------------------------------------------------------------------------
function breadcrumb() {
  global $post;
  global $post_type;
  global $taxonomy;
  global $term;

  $post_type = !empty( $post_type ) ? $post_type : 'post'; 

  $str = '';
  if( ( !is_home() || !is_front_page() ) && !is_admin() ) {
    $str .= '<div class="breadcrumb">';
    $str .= '<ul class="breadcrumb__list">';
    $str .= '<li class="breadcrumb__item"><a href="/">TOP</a></li>';
    if( is_page() ) { // 調整済み
      if( $post->post_parent != 0 ) {
        $ancestors = array_reverse( get_post_ancestors( $post->ID ) );
        foreach( $ancestors as $ancestor ) {
          $str .= '<li class="breadcrumb__item"><a href="'. get_permalink( $ancestor ) .'">'. get_the_title( $ancestor ) .'</a></li>';
        }
      }
      $str .= '<li class="breadcrumb__item">'. get_the_title() .'</li>';
    } elseif( is_singular( $post_type ) && is_single() ) {
      $link = $post_type !== 'post' ? get_post_type_archive_link( $post_type ) : '/info/';
      $txt  = $post_type !== 'post' ? esc_html( get_post_type_object( $post_type )->label ) : 'お知らせ';
      $str .= '<li class="breadcrumb__item"><a href="'. $link .'">';
      $str .= $txt;
      $str .= '</a></li>';
      $str .= '<li class="breadcrumb__item">'. get_the_title() .'</li>';
    } elseif( is_post_type_archive( $post_type ) ) {
      $str .= '<li class="breadcrumb__item">';
      $str .= esc_html( get_post_type_object( $post_type )->label );
      $str .= '</li>';
    } elseif( is_year() ) {
      $url  = $_SERVER["REQUEST_URI"];
      $url  = explode( '/', $url );
      //配列の中の空要素を削除する
      $url  = array_filter( $url, 'strlen' );
      //添字を振り直す
      $url  = array_values( $url );

      $str .= '<li class="breadcrumb__item">';
      $str .= esc_html( $url[0] );
      $str .= '</li>';
    } elseif( is_month() ) {
      $url  = $_SERVER["REQUEST_URI"];
      $url  = explode( '/', $url );
      //配列の中の空要素を削除する
      $url  = array_filter( $url, 'strlen' );
      //添字を振り直す
      $url  = array_values( $url );

      $str .= '<li class="breadcrumb__item">';
      $str .= '<a href="/'. $url[0] .'/">';
      $str .= $url[0];
      $str .= '</a>';
      $str .= '</li>';

      $str .= '<li class="breadcrumb__item">';
      $str .= esc_html( $url[1] );
      $str .= '</li>';
    } elseif( is_day() ) {
      $url  = $_SERVER["REQUEST_URI"];
      $url  = explode( '/', $url );
      //配列の中の空要素を削除する
      $url  = array_filter( $url, 'strlen' );
      //添字を振り直す
      $url  = array_values( $url );

      $str .= '<li class="breadcrumb__item">';
      $str .= '<a href="/'. $url[0] .'/">';
      $str .= $url[0];
      $str .= '</a>';
      $str .= '</li>';

      $str .= '<li class="breadcrumb__item">';
      $str .= '<a href="/'. $url[0] .'/'. $url[1] .'/">';
      $str .= $url[1];
      $str .= '</a>';
      $str .= '</li>';

      $str .= '<li class="breadcrumb__item">';
      $str .= esc_html( $url[2] );
      $str .= '</li>';
    } elseif( is_category() ) {
      $str .= '<li class="breadcrumb__item">';
      $cat  = get_category( get_query_var( 'cat' ), false );
      $str .= esc_html( $cat->name );
      $str .= '</li>';
    } elseif( is_tax( $taxonomy, $term ) ) {
      $str .= '<li class="breadcrumb__item">';
      $str .= get_taxonomy( $taxonomy )->label;
      $str .= '</li>';
      $str .= '<li class="breadcrumb__item">';
      $str .= single_tag_title( '', false );
      $str .= '</li>';
    } elseif( is_search() ) {
    } elseif( is_404() ) {
      $str .= '<li class="breadcrumb__item">404</li>';
    } else {
      $str .= '<li class="breadcrumb__item">お知らせ</li>';
    }
    $str .= '</ul>';
    $str .= '</div>';
  }
  echo $str;
}


//---------------------------------------------------------------------------------------------------
/**
 * 02.0 - カスタム投稿タイプ
 */
//---------------------------------------------------------------------------------------------------

/**
 * 02.1 - カスタム投稿の追加
 */

function create_post_type() {
  register_post_type( 'event', //タイプ名の指定
    array(
      'labels'           => array (
        'name'           => __( 'イベント登録' ),
        'singular_name'  => __( 'event' ),
      ),
      'public'        => true,
      'has_archive'   => true,
      'rewrite'       => array( 'slug' => 'event', ),
      'menu_position' => 5,
      'supports'      => array (
        'title',
        'editor',
        'thumbnail',
      ),
    )
  );
}
add_action( 'init', 'create_post_type' );

/**
 * 02.2 - カスタム投稿タイプのタクソノミー登録
 */

function event_taxonomies() {

  $labels = array(
    'name'           => 'イベント登録カテゴリ',
    'singular_name'  => 'event_cat',
  );

  register_taxonomy('event_cat', array( 'event' ),
    array(
      'hierarchical'      => true,
      'labels'            => $labels,
      'show_ui'           => true,
      'show_admin_column' => true,
      'show_in_nav_menus' => true,
    )
  );
}
// add_action( 'init', 'event_taxonomies', 0 );

/**
 * 02.3 - カスタム投稿タイプのスラッグをIDへ変更
 */

function custom_posttype_rewrite() {
  global $wp_rewrite;
  $wp_rewrite->add_rewrite_tag('%event%', '(event)','post_type=');
  $wp_rewrite->add_permastruct('event', '/%event%/%post_id%/', false);
}
// add_action('init', 'custom_posttype_rewrite');


function custom_posttype_permalink($post_link, $id = 0, $leavename) {

  global $wp_rewrite;
  $post = get_post($id);
  if(is_wp_error( $post )){
    return $post;
  }
  if('event' === $post->post_type){
    $newlink = $wp_rewrite->get_extra_permastruct($post->post_type);
    $newlink = str_replace('%event%', $post->post_type, $newlink);
    $newlink = str_replace('%post_id%', $post->ID, $newlink);
    $newlink = home_url(user_trailingslashit($newlink));
    return $newlink;
  }
  return $post_link;
}
// add_filter('post_type_link', 'custom_posttype_permalink', 1, 3);

//---------------------------------------------------------------------------------------------------
/**
 * 03.0 - アーカイブページの記事ループ外でも、通常の記事ループ内でもpost_typeを取得できるようにした関数
 */
//---------------------------------------------------------------------------------------------------

function get_post_type_query() {
  if ( is_archive() ) {
    return get_query_var( 'post_type' );
  }
  return get_post_type();
}

//---------------------------------------------------------------------------------------------------
/**
 * 04.0 - 子ページであるかのチェック(true -> 親ページの ID を返す。)
 */
//---------------------------------------------------------------------------------------------------

function is_subpage() {
  global $post;
  if ( is_page() && $post->post_parent ) {
    $parentID = $post->post_parent;
    // 親ページの ID を返す。
    return $parentID;
  } else {
    return false;
  };
};

//---------------------------------------------------------------------------------------------------
/**
 * 05.0 - 親スラッグの取得
 */
//---------------------------------------------------------------------------------------------------
/**
 * 05.1 - 直上の親を返す
 */
function is_parent_slug( $post_type ) {
  global $post;
  if ( $post->post_parent ) {
    $post_data = get_post( $post->post_parent );

    if ( empty( $post_type ) ) {
      return $post_data->post_name;
    }

    if ( !empty( $post_type ) && $post_data->post_name === $post_type ) {
      return true;
    } else {
      return false;
    }
  }
}

/**
 * 05.2 - 一番上の親を返す
 */
function is_root_parent_slug( $post_type ) {
  global $post;
  $root_parent = get_page($post->ancestors[count($post->ancestors) - 1]);

  if ( empty( $post_type ) ) {
    return $root_parent->post_name;
  }

  if ( !empty( $post_type ) && $root_parent->post_name === $post_type ) {
    return true;
  } else {
    return false;
  }
}

//---------------------------------------------------------------------------------------------------
/**
 * 06.0 - アイキャッチのサイズ追加
 */
//---------------------------------------------------------------------------------------------------

add_image_size( 'post_thumbnail', 420, 420, true );

// メディアアップローダーに登録するために必須
function my_custom_sizes( $sizes ) {
  return array_merge( $sizes, array(
    'post_thumbnail' => __('post_thumbnail'),
  ) );
}
add_filter( 'image_size_names_choose', 'my_custom_sizes' );
add_theme_support('post-thumbnails');

//サムネイルカラム追加
function customize_manage_posts_columns($columns) {
  $columns['thumbnail'] = __('Thumbnail');
  return $columns;
}
add_filter( 'manage_posts_columns', 'customize_manage_posts_columns' );
 
//サムネイル画像表示
function customize_manage_posts_custom_column($column_name, $post_id) {
  if ( 'thumbnail' == $column_name) {
      $thum = get_the_post_thumbnail($post_id, 'small', array( 'style'=>'width:100px;height:auto;' ));
  } if ( isset($thum) && $thum ) {
      echo $thum;
  } else {
      echo __('None');
  }
}
add_action( 'manage_posts_custom_column', 'customize_manage_posts_custom_column', 10, 2 );


//---------------------------------------------------------------------------------------------------
/**
 * 07.0 - ユーザー検索をリダイレクト
 */
//---------------------------------------------------------------------------------------------------

function author_archive_redirect() {
  if( is_author() ) {
    wp_redirect( home_url());
    exit;
  }
}
add_action( 'template_redirect', 'author_archive_redirect' );

//---------------------------------------------------------------------------------------------------
/**
 * 08.0 - All in one SEO 除外系
 */
//---------------------------------------------------------------------------------------------------

function deregister_aioseop_style() {
  wp_deregister_style( 'aioseop-toolbar-menu' );
}
add_action('wp_print_styles', 'deregister_aioseop_style', 100 );


//---------------------------------------------------------------------------------------------------
/**
 * 09.0 - デフォルトギャラリーのCSS停止
 */
//---------------------------------------------------------------------------------------------------

add_filter( 'use_default_gallery_style', '__return_false' );

?>
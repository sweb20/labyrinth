<?php get_header(); ?>

  <section class="sec__mv">
    <h2 class="sec__ttl"><span>BLOG</span></h2>
  </section>

  <div class="blog contents">

    <div class="blog__inner">
      <ul class="blog__list">
        <?php
        $paged = (int) get_query_var('paged');
        $args = array(
          'posts_per_page' => 6,
          'paged' => $paged,
          'orderby' => 'post_date',
          'order' => 'DESC',
          'post_type' => 'post',
          'post_status' => 'publish',
        );
        $the_query = new WP_Query($args);
        if ( $the_query->have_posts() ) :
          while ( $the_query->have_posts() ) : $the_query->the_post();
        ?>
          <li class="blog__item">
          	 <p class="blog__img"><a href="<?php the_permalink(); ?>"><?php
              // アイキャッチ
              if ( has_post_thumbnail( $post->ID ) ) {
                echo the_post_thumbnail( 'blog_thumbnail' );
              } else {
                echo '<img src="'.get_template_directory_uri().'/img/blog/no__image.jpg" alt="イメージがありません。">';
              }
            ?></a></p>
            
            <p class="blog__day"><?php echo get_the_date('Y.m.d'); ?></p>
            <h3 class="blog__ttl"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <p class="blog__content">
            <?php
              if ( mb_strlen( $post->post_content, 'UTF-8' ) > 45 ) {
                $content = mb_substr( strip_tags($post->post_content), 0, 45, 'UTF-8' );
                echo $content.'……';
              } else {
                echo strip_tags($post->post_content);
              }
              ?>
          </p>

            <?php
                // カテゴリーの取得

                // $terms = get_the_terms( get_the_id(), 'blog_cat' );
                $terms = get_the_category();
                $cat   = array();

                if ( $terms ) {
                  echo '<p class="blog__cat">';
                  // カテゴリーをセット
                  foreach ( $terms as $key => $value ) {
                    $cat[] = $value->name;
                    echo '<span><a href="/'.$value->taxonomy.'/'.$value->slug.'/">'.$value->name.'</a></span>';
                  }
                  echo '</p>';
                }
              ?>
          </li>
        <?php endwhile; endif; ?>
      </ul>
      <?php 
      if ( $the_query->max_num_pages > 1 ) {
        echo paginate_links(
          array(
            'base'    => get_pagenum_link(1) . '%_%',
            'format'  => 'page/%#%/',
            'current' => max(1, $paged),
            'total'   => $the_query->max_num_pages,
            'next_txt'=> '>',
            'type'    => 'list'
          )
        );
      }
      ?>
    </div>

    <aside class="side">

      <?php dynamic_sidebar( 'blog__widget' ); ?>
      
      <p class="side__banner__pc">
        <a href="https://px.a8.net/svt/ejp?a8mat=358S7L+AINPIQ+348+U6ODD" target="_blank" rel="nofollow">
        <img border="0" width="100%" height="auto" alt="" src="https://www24.a8.net/svt/bgt?aid=190206417636&wid=001&eno=01&mid=s00000000404005070000&mc=1"></a>
      </p>
      <p class="side__banner__sp">
        <a href="https://px.a8.net/svt/ejp?a8mat=358S7L+AINPIQ+348+U3GMP" target="_blank" rel="nofollow">
        <img border="0" width="468" height="60" alt="" src="https://www27.a8.net/svt/bgt?aid=190206417636&wid=001&eno=01&mid=s00000000404005055000&mc=1"></a>
      </p>
      
    </aside>

  </div><!-- /. blog-->
<?php get_footer(); ?>
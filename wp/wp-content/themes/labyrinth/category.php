<?php get_header(); ?>

<section class="sec__mv">
  <h2 class="sec__ttl"><span>カテゴリBLOG</span></h2>
</section>

<div class="blog">
  <div class="blog__inner">
    <ul class="blog__list">
      <?php
        if ( have_posts() ) :
          while ( have_posts() ) : the_post();
      ?>
        <li class="blog__item">
          <p class="blog__img"><a href="<?php the_permalink(); ?>"><?php
            // アイキャッチ
            if ( has_post_thumbnail( $post->ID ) ) {
              echo the_post_thumbnail( 'blog_thumbnail' );
            } else {
              echo '<img src="'.get_template_directory_uri().'/img/blog/no__image.jpg" alt="no__image">';
            }
          ?></a></p>
          <p class="blog__day"><?php echo get_the_date(); ?></p>
          <h4 class="blog__ttl"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
          <p class="blog__content">
            <?php
              if ( mb_strlen( $post->post_content, 'UTF-8' ) > 45 ) {
                $content = mb_substr( strip_tags($post->post_content), 0, 45, 'UTF-8' );
                echo $content.'……';
              } else {
                echo strip_tags($post->post_content);
              }
              ?>
          </p>
         
            <?php
              // カテゴリーの取得
              $terms = get_the_category();
              $cat   = array();

              if ( $terms ) {
                echo '<p class="blog__cat">';
                // カテゴリーをセット
                foreach ( $terms as $key => $value ) {
                  $cat[] = $value->name;
                  echo '<span><a href="/'.$value->taxonomy.'/'.$value->slug.'/">'.$value->name.'</a></span>';
                }
                echo '</p>';
              }
            ?>
         
        </li>
      <?php
          endwhile;
        endif;
      ?>
    </ul>
    <?php
      if ( $wp_query->max_num_pages > 1 ) {
        echo paginate_links(
          array(
            'base'    => get_pagenum_link(1) . '%_%',
            'format'  => 'page/%#%/',
            // 'current' => max(1, $paged),
            'total'   => $wp_query->max_num_pages,
            'next_txt'=> '>',
            'type'    => 'list'
          )
        );
      }
    ?>
  </div>

  <aside class="side">
    <div class="side__inner">
      <?php dynamic_sidebar( 'blog__widget' ); ?>
    </div>
  </aside>

</div>
<?php get_footer(); ?>
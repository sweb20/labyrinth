<?php
/*
Template Name: フロント
*/
?>

<?php get_header(); ?>

<!-- MV -->
  <div class="main_visual">
    <div class="main_visual_box ta_c">
      <figure>
        <img src="<?php echo get_template_directory_uri(); ?>/img/index/mv__pc.jpg" class="pc" alt="奇妙なラビリンスへの招待状">
        <img src="<?php echo get_template_directory_uri(); ?>/img/index/mv__sp.jpg" class="sp" alt="奇妙なラビリンスへの招待状">
      </figure>
      <div class="mv_txt">
        <img src="<?php echo get_template_directory_uri(); ?>/img/index/mv_txt01.png" alt="奇妙なラビリンスへの招待状">
      </div>
    </div>
  </div>

  <main class="main_wrap">
    <article>
      <!-- お知らせ -->
      <section id="news_section" class="news_section anchor">
        <div class="section_inner w840">
          <h2 class="title_circle01 rotateBox" >
            <p class="font26 yumin mb10">お知らせ</p>
            <span></span>
            <p class="loto italic">News</p>
          </h2>
          <ul class="news_wrap mt55">
            <?php 
              // 新着情報を件取得
              $query = array(
                'post_type'      => 'post',
                'post_status'    => 'publish',
                'posts_per_page' => 3,
                'order'          => 'DESC'
              );
              
              $the_query = new WP_Query( $query );

              if ( $the_query->have_posts() ) :
                while ( $the_query->have_posts() ) :
                  $the_query->the_post();
            ?>
            <li class="news_box">
              <div class="data">
                <p class="loto italic"><?php echo get_the_date('Y.m.d'); ?></p>
              </div>
              <div class="title">
                <h2 class="txt_set01 bold"><?php the_title(); ?></h2>
              </div>
            </li>
            <?php endwhile;
              endif;
              wp_reset_postdata(); // reset
            ?>
          </ul>
        </div>
      </section>

      <!-- イントロ -->
      <section id="introduction_section" class="anchor">
        <div class="introduction_section">
          <div class="para01 abs">
            <img class="para_up" src="<?php echo get_template_directory_uri(); ?>/img/index/img_para_introduction01.png" alt="羽の画像">
          </div>
          <div class="para02 abs">
            <img class="para_up_right" src="<?php echo get_template_directory_uri(); ?>/img/index/img_para_introduction02.png" alt="羽の画像">
          </div>
          <div class="para03 abs">
            <img class="para_up" src="<?php echo get_template_directory_uri(); ?>/img/index/img_para_introduction03.png" alt="羽の画像">
          </div>
          <div class="para04 abs">
            <img class="para_up_right" src="<?php echo get_template_directory_uri(); ?>/img/index/img_para_introduction04.png" alt="羽の画像">
          </div>
          <div class="section_inner">
            <h2 class="title_circle01 c_white">
              <p class="font26 yumin mb10">イントロダクション</p>
              <span></span>
              <p class="loto italic">Introduction</p>
            </h2>
            <div class="img_txt_box">
              <div class="img_box">
                <figure>
                  <img class="pc" src="<?php echo get_template_directory_uri(); ?>/img/index/intro__01--pc.jpg" alt="ウェブ解析士協会の代表理事が謎の男Kに誘拐された。">
                  <img class="sp" src="<?php echo get_template_directory_uri(); ?>/img/index/intro__01--sp.jpg" alt="ウェブ解析士協会の代表理事が謎の男Kに誘拐された。">
                </figure>
                <div class="flag">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/index/img_flag01.png" alt="はじまり">
                  <p class="font26 yumin">はじまり</p>
                </div>
              </div>
              <div class="txt_box">
                <div>
                  <p class="txt_set02 font18 c_white">ある日、あなたのところにウェブ解析士協会から1通の招待状が舞い込む。<br>オンラインイベントに参加するあなた。<br><br>そこで見たものは、ウェブ解析士協会の代表理事である江尻氏が謎の男Kに誘拐されたところだった。<br>Kは奇妙なラビリンスに仕掛けたAIで、彼から知識や経験を奪い取り、ウェブ解析士たちへ復讐するつもりだという。<br><br>このままでは江尻氏が復讐に利用され、殺されてしまう。</p>
                </div>
              </div>
            </div>
            <div class="mt50 img_txt_box">
              <div class="txt_box reverse order2_sp">
                <div>
                  <p class="txt_set02 font18 c_white">しかし、Kはあなたに1度だけチャンスを与えると言う。<br>「ウェブ解析士が本当に成果を出せる人間なのかを試すために、謎を用意した。<br>1時間以内に私の出した謎を解けば、理事を解放してやろう。」<br><br>すべての謎を解き明かし、ウェブ解析士の価値を証明することが出来るのか？<br><br>ウェブ解析士の価値をめぐる、世界一の謎の物語が今、幕を開ける。</p>
                </div>
              </div>
              <div class="img_box order1_sp">
                <figure>
                  <img class="pc" src="<?php echo get_template_directory_uri(); ?>/img/index/intro__02--pc.jpg" alt="ウェブ解析士協会の代表理事が謎の男Kに誘拐された。">
                  <img class="sp" src="<?php echo get_template_directory_uri(); ?>/img/index/intro__02--sp.jpg" alt="ウェブ解析士協会の代表理事が謎の男Kに誘拐された。">
                </figure>
                <div class="flag">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/index/img_flag01.png" alt="第1幕">
                  <p class="font26 yumin">第1幕</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- 公演情報 -->
      <section id="performance_section" class="anchor">
        <div class="performance_section">
          <div class="para01 abs">
            <img class="para_up_left" src="<?php echo get_template_directory_uri(); ?>/img/index/img_para_performance01.png" alt="羽の画像">
          </div>
          <div class="para02 abs">
            <img class="para_up " src="<?php echo get_template_directory_uri(); ?>/img/index/img_para_performance02.png" alt="羽の画像">
          </div>
          <div class="section_inner">
            <h2 class="title_circle01 c_white">
              <p class="font26 yumin mb10">公演情報</p>
              <span></span>
              <p class="loto italic">Performance</p>
            </h2>
            <div class="event_wrap mt45 flex wrap jt_center">
              <?php 
                // 新着情報を件取得
                $query = array(
                  'post_type'      => 'event',
                  'post_status'    => 'publish',
                  'posts_per_page' => 3,
                  'order'          => 'ASC'
                );
                
                $the_query = new WP_Query( $query );

                if ( $the_query->have_posts() ) :
                  while ( $the_query->have_posts() ) :
                    $the_query->the_post();
              ?>
              <div class="event_box">
              
                <div class="inner_box">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/index/bg_frame01.png">
                  <div class="detail_box">
                    <p class="detail_font40 loto bold"><?php the_field('day'); ?></p>
                    <p class="detail_font32 bold"><?php the_field('event_name'); ?></p>
                    <div class="flex wrap jt_center mb15">
                      <p class="detail_font15 mr10 bold">Zoom入場:<?php the_field('zoom_admission'); ?></p>
                      <p class="detail_font15 mb5 bold">開始時間:<?php the_field('start'); ?></p>
                      <p class="detail_font15 bold">終了時間:<?php the_field('end'); ?></p>
                    </div>
                    <div class="primary_button w80 auto">
                      <a class="bold" href="<?php the_field('url'); ?>"><?php the_field('btn_txt'); ?></a>
                    </div>
                  </div>
                </div>
               
              </div>
            <?php endwhile; endif; ?> 
            </div>
            <div class="border_wrap mt35 pt25 pb25 ta_c">
              <p class="label yumin">参加費</p>
              <p class="c_white mt10"><span class="font48 italic loto mr10">3,500</span><span class="yumin font28">円</span></p>
              <div class="flex jt_center ag_baseline mt20 mb20">
                <span class="icon_check mr10">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/index/ico_check01.png" alt="チェックアイコン">
                </span>
                <p class="c_white ta_l">当日、ご参加いただいた方でランダムに少人数のチーム分けを行います。</p>
              </div>
              <div class="flex jt_center ag_baseline mt20 mb20">
                <span class="icon_check mr10">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/index/ico_check01.png" alt="チェックアイコン">
                </span>
                <p class="c_white ta_l">Zoomでのオンラインイベントとなります。あらかじめアカウント（無料版で可）をご用意ください。</p>
              </div>
              <div class="flex jt_center ag_baseline">
                <span class="icon_check mr10">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/index/ico_check01.png" alt="チェックアイコン">
                </span>
                <p class="c_white ta_l">推奨ブラウザ : Google Chrome、Fire Fox、Brave、Microsoft Edge<br>※推奨以外でプレーなさる場合は挙動が保証できませんのでご了承ください。</p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- 予告編映像 -->
      <section id="message_section1" class="anchor">
        <div class="message_section">
          <div class="para01 abs">
            <img class="para_up_down" src="<?php echo get_template_directory_uri(); ?>/img/index/img_para_message01.png" alt="羽の画像">
          </div>
          <div class="para02 abs">
            <img class="para_up " src="<?php echo get_template_directory_uri(); ?>/img/index/img_para_message02.png" alt="羽の画像">
          </div>
          <div class="para03 abs">
            <img class="para_up_right " src="<?php echo get_template_directory_uri(); ?>/img/index/img_para_message03.png" alt="羽の画像">
          </div>
          <div class="section_inner w840">
            <!-- <h2 class="title_circle01 c_white">
              <p class="font26 yumin mb10">予告編映像</p>
              <span></span>
              <p class="loto italic">Trailer Movie</p>
            </h2>
            <div class="message_wrap mt50">
              <div class="youtube2">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/PgowA-6vYWQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div> -->
            <div id="message_section2" class="anchor">
              <h2 class="title_circle01 c_white mt60">
                <p class="font26 yumin mb10">主題歌：夜寝てる間に</p>
                <span></span>
                <p class="loto">Theme song</p>
              </h2>
              <div class="message_wrap mt50">
                <div class="youtube2">
                  <iframe width="560" height="315" src="https://www.youtube.com/embed/VYP5L7vrlZU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section id="cast_section" class="cast_section">
          <div class="section_inner_medium ta_c">
            <p class="font36 bold c_red mb40 font22_sp">出演：<br class="sp">窪田望　江尻俊章　くつざわ</p>
            <p class="font26 bold c_red mb30 font15_sp">企画：窪田望 / 脚本：中村祐貴子 / 監督：牟田朋晃 / アドバイザー：飯野慎也<br>
              ヘアメイク : Riina / スタイリスト：松野仁美 / アシスタント：三尋木椋太<br>
            サイト制作：山内真由美</p>
            <p class="font26 bold c_red mb30 font15_sp">主題歌：夜寝てる間に</p>
            <p class="font26 bold c_red font15_sp"><a href="https://www.waca.associates/jp/" target="_blank" class="c_red">協力：一般社団法人ウェブ解析士協会</a> 、 <a href="https://kobit.in/" target="_blank" class="c_red">KOBIT</a><br>
            ロケ協力：スタジオキコリ</p>
          </div>
        </section>

        <p class="entry__button">
          <a class="bold" href="<?php the_field('url'); ?>"><?php the_field('btn_txt'); ?></a>
        </p>
      </article>

    <section id="gllery" class="anchor">
      <?php echo do_shortcode( '[gallery link="file" columns="4" size="full" ids="16,17,18,19,24,25,26,27,53,54,55,56,57,58,59,61"]' ); ?>
    </section>

<?php get_footer(); ?>
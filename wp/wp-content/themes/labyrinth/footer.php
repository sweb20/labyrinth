
  	<footer class="footer">
      <div class="footer_wrap flex wrap jt_between">
        <div class="logo_area">
          <img src="<?php echo get_template_directory_uri(); ?>/img/common/footer_logo.svg" alt="奇妙なラビリンスへの招待状">
          <p class="c_red font18 bold mt15">運営 : 株式会社クリエイターズネクスト</p>
          <a class="red_link font18 bold" href="https://cnxt.jp/" target="_blank">https://cnxt.jp/</a>
        </div>
        <div class="sns_area flex wrap jt_end">
          <ul class="footer_sns flex jt_end">
            <li>
              <a href="https://www.instagram.com/wacajp/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/common/icon_instagram.svg" alt="インスタ「奇妙なラビリンスへの招待状」君は解けるかな？"></a>
            </li>
            <li>
              <a href="http://bit.ly/nazo1031tw" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/common/icon_twitter.svg" alt="ツイッター「奇妙なラビリンスへの招待状」君は解けるかな？"></a>
            </li>
            <li>
              <a href="https://bit.ly/2ECIXlU" target="_blank" rel="nofollow noopener noreferrer"><img src="<?php echo get_template_directory_uri(); ?>/img/common/icon_facebook.svg" alt="Facebook「奇妙なラビリンスへの招待状」君は解けるかな？"></a>
            </li>
          </ul>
          <p class="copy flex ag_bottom c_red bold italic">&copy;<span class="font17 loto font14_sp">奇妙なラビリンスへの招待状運営事務局(2020)</span></p>
        </div>
      </div>
    </footer>
  </main>

  <!-- js -->
  <script src="https://code.jquery.com/jquery-2.2.4.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/simple-parallax-js@5.5.1/dist/simpleParallax.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/common.js"></script>
  <script type="text/javascript">
    $(function(){
      let para_up = document.getElementsByClassName('para_up');
      new simpleParallax(para_up, {
        orientation:'up',
        overflow: true,
        delay: 1.8,
        scale:1.8
      });
      let para_up_right = document.getElementsByClassName('para_up_right');
      new simpleParallax(para_up_right, {
        orientation:'up-right',
        overflow: true,
        delay: 1.8,
        scale:1.8
      });
      let para_up_left = document.getElementsByClassName('para_up_left');
      new simpleParallax(para_up_left, {
        orientation:'up-left',
        overflow: true,
        delay: 1.8,
        scale:1.8
      });
      let para_up_down = document.getElementsByClassName('para_up_down');
      new simpleParallax(para_up_down, {
        orientation:'down-left',
        overflow: true,
        delay: 1.8,
        scale:1.8
      });
    })
  </script>
  
</body>
</html>
